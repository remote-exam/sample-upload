$(document).ready(function(e){
    
    $("#uploadModal").on('click', function() {
        $("#upload-modal").modal();
    });

    $("#upload-form").on('submit', function(e) {
        
        e.preventDefault();
        
        var formdata = new FormData(this);
        var files = $('#file')[0].files;
        if(files.length > 0) {
            formdata.append('file', files[0]);

            $.ajax({
                type: 'POST',
                url: "upload.php",
                data: formdata, // $("#upload-form").serialize(),
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (response) {
                    var img_src = response.thumbnail;
                    var newRow = "<tr>"+
                        "<td>"+response.title+"</td><td>"+
                        "<img src=\""+ img_src +"\"</td>"+
                        "<td>"+response.filename+"</td>"+
                        "<td>"+response.date_added+"</td>"+
                        "<td><i class=\"fas fa-trash-alt\" style=\"font-size:48px;\"></i></td></tr>";

                    $('#mediaFiles').append(newRow);
                    $('#upload-modal').modal('hide');
                },
                error: function() {
                    alert("failed");
                }
                })  
        } else {
            alert("Please select file");
        }
    });
});