<?php
    $record = [];

    if(isset($_FILES)) {
        $file = $_FILES['file'];
        $srcFileDir = $file['tmp_name'];
        $targetFileDir = __DIR__."\\files\\";
        move_uploaded_file($srcFileDir, $targetFileDir.$file['name']);
        
        if(file_exists($targetFileDir.$file['name'])){
            $record = [
                'title' => $file['name'],
                'thumbnail' => $targetFileDir.$file['name'],
                'filename' => $file['name'],
                'date_added' => date('Y-m-d')
            ];
        }
    }

    echo json_encode($record);
    
?>